import React, { Component, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.scss";

const loading = () => <div>Loading...</div>;

const Home = React.lazy(() => import("./pages/Home/Home"));
const State = React.lazy(() => import("./pages/State/State"));
const Prop = React.lazy(() => import("./pages/Prop/Prop"));
const Environment = React.lazy(() => import("./pages/Environment/Environment"));
const Login = React.lazy(() => import("./pages/Login/Login"));

export default class App extends Component {
  public render(): JSX.Element {
    return (
      <Router>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/state">State</Link>
          </li>
          <li>
            <Link to="/prop">Prop</Link>
          </li>
          <li>
            <Link to="/environment">Environment</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
        </ul>

        <Suspense fallback={loading()}>
          <Switch>
            <Route path="/state">
              <State />
            </Route>
            <Route path="/prop">
              <Prop />
            </Route>
            <Route path="/environment">
              <Environment />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Suspense>
      </Router>
    );
  }
}
