import React, { Component } from "react";

export default class Environment extends Component {
  constructor(props: any) {
    super(props);
  }

  public render(): JSX.Element {
    return (
      <div>
        <h1>Environment</h1>
        <h2>{process.env.REACT_APP_ENVIRONMENT}</h2>
        <h3>{process.env.REACT_APP_API}</h3>
      </div>
    );
  }
}
