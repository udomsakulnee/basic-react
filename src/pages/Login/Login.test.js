import React, { InputHTMLAttributes } from "react";
import { render, fireEvent } from "@testing-library/react";
import Login from "./Login";

describe("การเข้าสู่ระบบ", () => {
  let inputUsername;
  let inputPassword;
  let buttonLogin;
  let textAlert;

  beforeEach(() => {
    const { getByTestId } = render(<Login />);

    inputUsername = getByTestId("input-username");
    inputPassword = getByTestId("input-password");
    buttonLogin = getByTestId("button-login");
    textAlert = getByTestId("text-alert");
  });

  describe("ตรวจสอบค่าเริ่มต้น", () => {
    it("username เป็นค่าว่าง", () => {
      expect(inputUsername.value).toBe("");
    });

    it("password เป็นค่าว่าง", () => {
      expect(inputPassword.value).toBe("");
    });
  });

  describe("ตรวจสอบฟอร์ม", () => {
    it("ชื่อ input username", () => {
      expect(inputUsername.placeholder).toBe("Username");
    });

    it("type username", () => {
      expect(inputUsername.type).toBe("text");
    });

    it("ชื่อ input password", () => {
      expect(inputPassword.placeholder).toBe("Password");
    });

    it("type password", () => {
      expect(inputPassword.type).toBe("password");
    });

    it("ชื่อ button login", () => {
      expect(buttonLogin.textContent).toBe("Login");
    });

    it("type button", () => {
      expect(buttonLogin.type).toBe("button");
    });
  });

  describe("คลิกปุ่ม Login", () => {
    it("ไม่กรอกอะไรเลย จะมีข้อความแจ้งเตือน", () => {
      fireEvent.click(buttonLogin);
      expect(textAlert.textContent).toBe("กรุณาระบุ Username และ Password");
    });

    it("กรอก Username จะมีข้อความแจ้งเตือน", () => {
      fireEvent.change(inputUsername, { target: { value: "foo" } });
      fireEvent.click(buttonLogin);
      expect(textAlert.textContent).toBe("กรุณาระบุ Password");
    });

    it("กรอก Password จะมีข้อความแจ้งเตือน", () => {
      fireEvent.change(inputPassword, { target: { value: "bar" } });
      fireEvent.click(buttonLogin);
      expect(textAlert.textContent).toBe("กรุณาระบุ Username");
    });

    it("กรอก Username และ Password ครบถ้วน จะไม่มีข้อความแจ้งเตือน", () => {
        fireEvent.change(inputUsername, { target: { value: "foo" } });
        fireEvent.change(inputPassword, { target: { value: "bar" } });
        fireEvent.click(buttonLogin);
        expect(textAlert.textContent).toBe("");
      });
  });
});
