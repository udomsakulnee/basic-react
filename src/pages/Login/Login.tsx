import React, { useState } from "react";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [textAlert, setTextAlert] = useState("");

  const onClick = () => {
    setTextAlert("");
    if (username == "" && password == "") {
        setTextAlert("กรุณาระบุ Username และ Password");
    }
    else if (username == "") {
        setTextAlert("กรุณาระบุ Username");
    }
    else if (password == "") {
        setTextAlert("กรุณาระบุ Password");
    }

    alert(username + " " + password);
  };

  return (
    <div>
      Username:
      <input
        data-testid="input-username"
        type="text"
        placeholder="Username"
        value={username}
        onChange={e => setUsername(e.target.value)}
      />
      <br />
      Password:
      <input
        data-testid="input-password"
        type="password"
        placeholder="Password"
        value={password}
        onChange={e => setPassword(e.target.value)}
      />
      <br />
      <p data-testid="text-alert">{textAlert}</p>
      <button data-testid="button-login" type="button" onClick={onClick}>
        Login
      </button>
    </div>
  );
};
export default Login;