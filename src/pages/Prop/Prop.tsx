import React, { Component } from "react";
import Input from "../../components/Input/Input";

interface State {
    data: string;
    data2: string;
}

export default class Prop extends Component<{}, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            data: "foo",
            data2: ""
        }
    }

    public render(): JSX.Element {
        return (
            <div>
                <label>Foo: { this.state.data }</label>
                <Input value={this.state.data} valueChange={(value: string) => this.setState({ data: value })} type="text" placeholder="please input data" />
                <label>Bar: { this.state.data2 }</label>
                <Input value={this.state.data2} valueChange={(value: string) => this.setState({ data2: value })} type="text" placeholder="please input data" />
            </div>
        )
    }
}