import React, { Component } from "react";
import "./State.scss";

interface State {
  dynamicNum: number;
  message: string;
}
export default class App extends Component<{}, State> {
  private text: string = "ข้อความ";
  private num: number = 0;

  constructor(props: any) {
    super(props);

    this.state = {
      dynamicNum: 0,
      message: "ข้อความ"
    };

    setInterval(() => {
      this.num++;
      console.log("number ", this.num);
    }, 1000);
  }

  public increase(): void {
    this.setState({ dynamicNum: this.state.dynamicNum + 1 });
  }

  public render(): JSX.Element {
    return (
      <div className="state-page">
        <p>{this.text}</p>
        <p>Number: {this.num}</p>
        <p>State: {this.state.dynamicNum}</p>
        <p className="c-red">ทดสอบการแสดงผล</p>
        <button type="button" onClick={() => this.increase()}>
          add value
        </button>
      </div>
    );
  }
}