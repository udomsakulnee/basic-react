import React, { Component } from "react";

interface Props {
  value: any;
  valueChange: any;
  type?: string;
  placeholder?: string;
}

interface State {
  textAlert: string;
}

export default class Input extends Component<Props, State> {
  constructor(props: any) {
    super(props);

    this.state = {
        textAlert: ""
    }
  }

  public componentDidMount(): void {
      this.onChange(this.props.value);
  }

  public onChange(value: any): void {
    this.props.valueChange(value);

    if (value == "") {
      this.setState({ textAlert: "please input data" });
    } else {
      this.setState({ textAlert: "" });
    }
  }

  public render(): JSX.Element {
    return (
      <div>
        <input
          value={this.props.value}
          onChange={e => this.onChange(e.target.value)}
          type={this.props.type}
          placeholder={this.props.placeholder}
        />
        <p>{this.state.textAlert}</p>
      </div>
    );
  }
}
